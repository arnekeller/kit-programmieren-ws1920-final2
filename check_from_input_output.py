import sys

fuzz = sys.argv[1]

input = open(fuzz + "_input.txt", "r")
output = open(fuzz + "_output.txt", "r")

in_lines = input.readlines()
out_lines = output.readlines()
for i in range(0, len(in_lines)):
    if not in_lines[i].startswith("#"):
        print(">", in_lines[i], end="")
    else:
        print(in_lines[i], end="")
    if out_lines[i].startswith("Error, "):
        print("<e")
    else:
        print(out_lines[i], end="")

for i in range(len(in_lines), len(out_lines)):
    line = out_lines[i]
    if line.startswith("Error, "):
        print("<e")
    else:
        print(line, end="")
