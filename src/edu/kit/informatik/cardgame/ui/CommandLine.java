package edu.kit.informatik.cardgame.ui;

import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.command.Command;
import edu.kit.informatik.cardgame.ui.command.CommandFactory;
import edu.kit.informatik.Terminal;

/**
 * Interactive game runner, gets user inputs and processes {@link Command commands} specified.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame
 */
public final class CommandLine {
    /**
     * Command used to quit the game and terminate the program.
     */
    private static final String QUIT = "quit";
    /**
     * Output text indicating that the player lost the active game.
     *
     * @see CardGame#gameLost
     */
    private static final String LOST = "lost";

    /**
     * Utility class -> private constructor.
     */
    private CommandLine() {

    }

    /**
     * Start a new interactive user session. Returns when terminal input is fully processed or user {@link #QUIT exits}.
     */
    public static void startInteractive() {
        // create a new simulation
        final CardGame simulation = new CardGame();
        // whether the player lost
        boolean lost = false;
        // accept user input indefinitely
        while (true) {
            final String input = Terminal.readLine();

            // handle quit command
            if (input.startsWith(QUIT)) {
                if (input.length() != QUIT.length()) {
                    Terminal.printError("input after quit command");
                    continue; // skip invalid quit command
                } else {
                    return; // quit main loop otherwise
                }
            }

            final Command command;
            // attempt to parse user input
            try {
                command = CommandFactory.getCommand(input);
            } catch (final NumberFormatException | InvalidInputException e) {
                Terminal.printError(e.getMessage());
                continue; // to next command
            }
            // attempt to execute command
            try {
                command.apply(simulation);
            } catch (final LogicException e) {
                Terminal.printError(e.getMessage());
            }
            // inform the player if they lost
            final boolean gameNowLost = simulation.gameLost();
            // (only tell them if they lost after last action)
            if (gameNowLost && !lost) {
                Terminal.printLine(LOST);
            }
            lost = gameNowLost;
        }
    }
}
