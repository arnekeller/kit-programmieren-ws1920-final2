package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

/**
 * Draw command used to take a card from the stack. Will print the drawn card to the terminal.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame#draw
 */
public final class Draw extends Command {
    /**
     * Name of this command.
     */
    public static final String NAME = "draw";

    @Override
    public void apply(CardGame game) throws LogicException {
        Terminal.printLine(game.draw());
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (!input.equals(NAME)) {
            throw new InvalidInputException("invalid draw argument: none expected");
        }
    }
}
