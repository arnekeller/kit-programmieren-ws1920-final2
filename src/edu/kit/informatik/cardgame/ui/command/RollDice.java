package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Roll dice command.
 * Passes dice roll to the game and prints the result.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame#rollDice
 */
public final class RollDice extends Command {
    /**
     * Name of this command.
     */
    public static final String NAME = "rollD";
    private static final Pattern ROLL_DICE_ARGUMENTS = Pattern.compile(NAME + "\\+?(\\d+) \\+?(\\d+)");

    /**
     * Size of the dice rolled.
     */
    private int size;
    /**
     * User-supplied result of the dice roll.
     */
    private int rolledNumber;

    @Override
    public void apply(CardGame game) throws LogicException {
        Terminal.printLine(game.rollDice(size, rolledNumber));
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = ROLL_DICE_ARGUMENTS.matcher(input);
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid roll dice syntax");
        }
        size = Integer.parseInt(matcher.group(1));
        rolledNumber = Integer.parseInt(matcher.group(2));
    }
}
