package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.Item;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Build command. Allows the player to attempt to build a single item.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame#build
 */
public final class Build extends Command {
    /**
     * Name of this command.
     */
    public static final String NAME = "build";
    private static final Pattern BUILD_ARGUMENT = Pattern.compile(" (\\w+)");

    /**
     * Item to build.
     */
    private Item item;

    @Override
    public void apply(CardGame game) throws LogicException {
        final String result = game.build(item);
        if (result == null) {
            Terminal.printError("could not build item");
        } else {
            Terminal.printLine(result);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = Build.BUILD_ARGUMENT.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid build command");
        }
        item = Item.parse(matcher.group(1));
        if (item == null) { // parse could not process the item name
            throw new InvalidInputException("invalid item name");
        }
    }
}
