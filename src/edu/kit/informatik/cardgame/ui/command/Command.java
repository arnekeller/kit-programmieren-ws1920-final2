package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

/**
 * Command that can be applied to a card game.
 * Commands are implemented as separate classes for easy modification and expansion.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CommandFactory
 */
public abstract class Command {
    /**
     * Apply this command to a game.
     *
     * @param game game to use
     * @throws LogicException on semantically wrong user input
     */
    public abstract void apply(CardGame game) throws LogicException;

    /**
     * Parse user input into this command object.
     *
     * @param input one line of user input
     * @throws InvalidInputException if user input is in any way invalid
     */
    public abstract void parse(String input) throws InvalidInputException;
}
