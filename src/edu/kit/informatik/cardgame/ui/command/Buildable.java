package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.Item;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

import java.util.Comparator;
import java.util.Set;

/**
 * Command used to print a list of items the player can currently {@link Build build}.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame#getBuildableItems
 */
public final class Buildable extends Command {
    /**
     * Name of this command.
     */
    public static final String NAME = "build?";

    @Override
    public void apply(CardGame game) throws LogicException {
        final Set<Item> buildable = game.getBuildableItems();
        if (buildable.isEmpty()) {
            Terminal.printLine("EMPTY");
        } else {
            buildable.stream().sorted(Comparator.comparing(Object::toString)).forEach(Terminal::printLine);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (!input.equals(NAME)) {
            throw new InvalidInputException("invalid build? argument: none expected");
        }
    }
}
