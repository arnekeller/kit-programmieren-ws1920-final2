package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

/**
 * Reset command.
 * Removes player progress and restores the original card stack.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame#reset
 */
public final class Reset extends Command {
    /**
     * Name of this command.
     */
    public static final String NAME = "reset";

    @Override
    public void apply(CardGame game) throws LogicException {
        game.reset();
        Terminal.printLine("OK");
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (!input.equals(NAME)) {
            throw new InvalidInputException("invalid reset argument: none expected");
        }
    }
}
