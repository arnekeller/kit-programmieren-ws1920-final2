package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.cardgame.model.Card;
import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

import java.util.Collection;

/**
 * list-resources command.
 * Prints all of the {@link Card resources} collected by the player, starting with the first picked up resource.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame#getResources
 */
public final class ListResources extends Command {
    /**
     * Name of this command.
     */
    public static final String NAME = "list-resources";

    @Override
    public void apply(CardGame game) throws LogicException {
        final Collection<Card> resources = game.getResources();
        if (resources.isEmpty()) {
            // player does not own any resources
            Terminal.printLine("EMPTY");
        } else {
            resources.forEach(Terminal::printLine);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (!input.equals(NAME)) {
            throw new InvalidInputException("invalid list-resources argument: none expected");
        }
    }
}
