package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.Item;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

import java.util.List;

/**
 * list-buildings command.
 * Prints buildings of the player on the terminal, starting with the most recently built {@link Item}.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame#getItems
 */
public final class ListBuildings extends Command {
    /**
     * Name of this command.
     */
    public static final String NAME = "list-buildings";

    @Override
    public void apply(CardGame game) throws LogicException {
        final List<Item> items = game.getItems();
        if (items.isEmpty()) { // player has not built any items yet
            Terminal.printLine("EMPTY");
        } else {
            for (int i = items.size() - 1; i >= 0; i--) {
                Terminal.printLine(items.get(i));
            }
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (!input.equals(NAME)) {
            throw new InvalidInputException("invalid list-buildings argument: none expected");
        }
    }
}
