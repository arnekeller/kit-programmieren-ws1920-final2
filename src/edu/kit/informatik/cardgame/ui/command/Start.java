package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.cardgame.model.Card;
import edu.kit.informatik.cardgame.model.CardGame;
import edu.kit.informatik.cardgame.model.LogicException;
import edu.kit.informatik.cardgame.ui.InvalidInputException;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Start command.
 * Initializes the game with a card stack.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CardGame#start
 */
public final class Start extends Command {
    /**
     * Name of this command.
     */
    public static final String NAME = "start";
    private static final int EXPECTED_CARD_AMOUNT = 64;
    private static final String CARD_SEPARATOR = ",";
    private static final Pattern START_ARGUMENTS
            = Pattern.compile(NAME + " ((\\w+" + CARD_SEPARATOR + "){" + (EXPECTED_CARD_AMOUNT - 1) + "}(\\w+))");

    /**
     * Cards that should be used to start the game.
     */
    private Deque<Card> cards;

    @Override
    public void apply(CardGame game) throws LogicException {
        // attempt to start the game
        if (game.start(cards)) {
            Terminal.printLine("OK");
        } else {
            // game already started
            throw new LogicException("could not start game: not inactive");
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = START_ARGUMENTS.matcher(input);
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid start arguments");
        }
        cards = new ArrayDeque<>();
        for (final String s : matcher.group(1).split(CARD_SEPARATOR)) {
            final Card card = Card.parse(s);
            if (card == null) {
                throw new InvalidInputException("invalid start argument value(s)");
            }
            cards.add(card);
        }
    }
}
