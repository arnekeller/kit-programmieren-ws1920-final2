package edu.kit.informatik.cardgame.ui.command;

import edu.kit.informatik.cardgame.ui.InvalidInputException;

import java.util.TreeMap;
import java.util.function.Supplier;

/**
 * Factory (utility class) used to parse user input into commands.
 *
 * @author Arne Keller
 * @version 1.2
 */
public final class CommandFactory {
    /**
     * Collection of command suppliers keyed by their name/prefix.
     */
    private static final TreeMap<String, Supplier<Command>> COMMANDS = new TreeMap<>();

    static {
        // initialize registered commands
        COMMANDS.put(Start.NAME, Start::new);
        COMMANDS.put(Draw.NAME, Draw::new);
        COMMANDS.put(ListResources.NAME, ListResources::new);
        COMMANDS.put(Build.NAME, Build::new);
        COMMANDS.put(ListBuildings.NAME, ListBuildings::new);
        COMMANDS.put(Buildable.NAME, Buildable::new);
        COMMANDS.put(RollDice.NAME, RollDice::new);
        COMMANDS.put(Reset.NAME, Reset::new);
    }

    /**
     * Utility class -> private constructor.
     */
    private CommandFactory() {

    }

    /**
     * Parse a single line of user input into one command.
     *
     * @param input user input line
     * @return a fully specified command object
     * @throws InvalidInputException if user input is invalid
     */
    public static Command getCommand(String input) throws InvalidInputException {
        final Supplier<Command> commandSupplier = COMMANDS
                // match longest prefix
                .descendingKeySet().stream()
                .filter(input::startsWith)
                .map(COMMANDS::get)
                .findFirst().orElse(null);
        if (commandSupplier != null) {
            final Command command = commandSupplier.get();
            // attempt to parse with the correct command object
            command.parse(input);
            return command;
        } else { // no matching command found
            throw new InvalidInputException("unknown command");
        }
    }
}
