package edu.kit.informatik.cardgame.ui;

import edu.kit.informatik.cardgame.ui.command.Command;
import edu.kit.informatik.cardgame.ui.command.CommandFactory;

/**
 * Thrown on syntactically invalid user input, usually in a command {@link Command#parse parsers}.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CommandFactory
 */
public class InvalidInputException extends Exception {
    /**
     * Construct a new {@link InvalidInputException} with the specified message.
     *
     * @param message error message
     */
    public InvalidInputException(String message) {
        super(message);
    }
}
