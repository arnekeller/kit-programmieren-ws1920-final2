package edu.kit.informatik.cardgame;

import edu.kit.informatik.cardgame.ui.CommandLine;

/**
 * Main program class.
 *
 * @author Arne Keller
 * @version 1.0
 * @see CommandLine
 */
public final class Main {
    /**
     * Utility class -> private constructor.
     */
    private Main() {

    }

    /**
     * Program entry point.
     *
     * @param args command-line arguments (will be ignored)
     */
    public static void main(String[] args) {
        CommandLine.startInteractive();
    }
}
