package edu.kit.informatik.cardgame;

import edu.kit.informatik.Terminal;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    void game1() throws IOException {
        cmpInOut("game1_input.txt", "game1_output.txt");
    }

    @Test
    void game2() throws IOException {
        cmpInOut("game2_input.txt", "game2_output.txt");
    }

    @Test
    void commands1() throws IOException {
        cmpInOut("commands1_input.txt", "commands1_output.txt");
    }

    @Test
    void shackTest() throws IOException {
        cmpInOut("shack_test_input.txt", "shack_test_output.txt");
    }

    @Test
    void gameOverNoActions() throws IOException {
        cmpInOut("game_over_no_actions_input.txt", "game_over_no_actions_output.txt");
    }

    @Test
    void gameOverNoMaterials() throws IOException {
        cmpInOut("game_over_no_materials_input.txt", "game_over_no_materials_output.txt");
    }

    @Test
    void gameOverAfterAnimal() throws IOException {
        cmpInOut("game_over_after_animal_input.txt", "game_over_after_animal_output.txt");
    }

    @Test
    void gameOverAfterHangglider() throws IOException {
        cmpInOut("game_over_after_hangglider_input.txt", "game_over_after_hangglider_output.txt");
    }

    @Test
    void genericTest() throws IOException {
        cmpInOut("generic_test_input.txt", "generic_test_output.txt");
    }

    @Test
    void genericWAF2Test() throws IOException {
        cmpInOut("generic_waf2_test_input.txt", "generic_waf2_test_output.txt");
    }

    @Test
    void testCoverage() throws IOException {
        cmpInOut("testcoverage_input.txt", "testcoverage_output.txt");
    }

    @Test
    void integerFormat() throws IOException {
        cmpInOut("integer_format_input.txt", "integer_format_output.txt");
    }

    private void cmpInOut(String in, String out) throws IOException {
        System.setIn(new ByteArrayInputStream(readFile(in)));
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        Terminal.reload();
        Main.main(null);
        String outputStr = output.toString();
        String expected = new String(readFile(out));
        String[] expectedLines = expected.split(System.lineSeparator());
        String[] actualLines = outputStr.split(System.lineSeparator());
        if (expectedLines.length != actualLines.length) {
            assertEquals(expected, output.toString());
        } else {
            for (int i = 0; i < actualLines.length; i++) {
                if (expectedLines[i].startsWith("<e") || expectedLines[i].startsWith("Error, ")) {
                    if (!actualLines[i].startsWith("Error, ")) {
                        assertEquals(expected, output.toString());
                    }
                } else if (!expectedLines[i].equals(actualLines[i])) {
                    assertEquals(expected, output.toString());
                }
            }
        }
    }

    private byte[] readFile(String name) throws IOException {
        File file = new File(name);
        return Files.readAllBytes(file.toPath());
    }
}