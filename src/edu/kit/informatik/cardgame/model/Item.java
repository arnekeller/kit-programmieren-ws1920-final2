package edu.kit.informatik.cardgame.model;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static edu.kit.informatik.cardgame.model.Card.METAL;
import static edu.kit.informatik.cardgame.model.Card.PLASTIC;
import static edu.kit.informatik.cardgame.model.Card.WOOD;

/**
 * Game item that can be built by the player. Categorized into several {@link ItemCategory categories}.
 * An item can give a {@link #fightingBonus fighting bonus} against {@link CardCategory#ANIMAL animals},
 * {@link #itemsSecured secure items} or let the player {@link ItemCategory#ESCAPE escape}.
 * Some items require a dice to be rolled (see {@link #activate(CardGame, int, int)}).
 *
 * @see ItemCategory
 * @author Arne Keller
 * @version 1.1
 */
public enum Item implements RequireDice {
    /**
     * Axe. Provides an {@link #fightingBonus attack bonus} of two.
     * Should be used in preference to the {@link #CLUB club} if available.
     */
    AXE,
    /**
     * Club. Provides an {@link #fightingBonus attack bonus} of one.
     */
    CLUB,
    /**
     * Shack. Can {@link #itemsSecured save} the last five items in case of {@link CardCategory#ANIMAL animal} attacks
     * or {@link CardCategory#CATASTROPHE catastrophic events}.
     */
    SHACK,
    /**
     * Fireplace. {@link #itemsNeededToBuild Required} for some advanced items.
     */
    FIREPLACE,
    /**
     * Sailing raft. Can be used to {@link #activate(CardGame, int, int) attempt} an escape.
     */
    SAILING_RAFT,
    /**
     * Hang glider. Can be used to attempt to escape.
     */
    HANG_GLIDER,
    /**
     * Steam boat. Can be used to escape. Requires a fireplace to build.
     */
    STEAMBOAT,
    /**
     * A misspelled hot-air balloon. Requires a fireplace to build.
     */
    BALLON;
    // note: new items *require* changes in parse and toString
    // addition in resourcesNeeded heavily recommended

    /**
     * Return value indicating success.
     *
     * @see #activate
     */
    public static final String OK = "OK";
    /**
     * Return value indicating that the player won.
     *
     * @see #activate
     * @see ItemCategory#ESCAPE
     */
    public static final String WIN = "win";
    /**
     * Return value indicating that the player failed to escape.
     *
     * @see #activate(CardGame, int, int)
     * @see ItemCategory#ESCAPE
     */
    public static final String LOSE = "lose";

    /**
     * Activate this item on the specified game.
     *
     * @param game card game to use
     * @return result (either {@link #OK} or {@link #WIN})
     */
    public String activate(CardGame game) {
        if (this.category() == ItemCategory.ESCAPE) {
            if (this.diceSizeNeeded().isPresent()) {
                // player needs to roll dice to escape
                game.waitForDiceRoll(this);
            } else {
                // player won
                game.winGame();
                return WIN;
            }
        }
        return OK;
    }

    @Override
    public Optional<String> activate(CardGame game, int size, int roll) {
        if (!this.diceSizeNeeded().isPresent()) {
            throw new IllegalStateException("can not process dice roll");
        } else if (this.diceSizeNeeded().get() != size || roll > size || roll < 1) {
            return Optional.empty();
        }

        if (roll >= this.minimumDiceRollNeeded().get()) {
            game.winGame();
            return Optional.of(WIN);
        } else {
            return Optional.of(LOSE);
        }
    }

    /**
     * Get the resources needed to build this item, in no particular order.
     *
     * @return resources needed to build this item
     */
    public Card[] resourcesNeeded() {
        switch (this) {
            case AXE:
                return new Card[] {METAL, METAL, METAL};
            case CLUB:
                return new Card[] {WOOD, WOOD, WOOD};
            case SHACK:
                return new Card[] {WOOD, WOOD, METAL, PLASTIC, PLASTIC};
            case FIREPLACE:
                return new Card[] {WOOD, WOOD, WOOD, METAL};
            case SAILING_RAFT:
                return new Card[] {WOOD, WOOD, WOOD, WOOD, METAL, METAL, PLASTIC, PLASTIC};
            case HANG_GLIDER:
                return new Card[] {WOOD, WOOD, METAL, METAL, PLASTIC, PLASTIC, PLASTIC, PLASTIC};
            case STEAMBOAT:
                return new Card[] {METAL, METAL, METAL, METAL, METAL, METAL, PLASTIC};
            case BALLON:
                return new Card[] {WOOD, PLASTIC, PLASTIC, PLASTIC, PLASTIC, PLASTIC, PLASTIC};
            default:
                return new Card[0];
        }
    }

    /**
     * Items required to be able to build this item. Building this item does not consume the items returned.
     *
     * @return items required to build this item
     */
    public Collection<Item> itemsNeededToBuild() {
        switch (this) {
            case BALLON:
            case STEAMBOAT:
                return Collections.singletonList(FIREPLACE);
            default:
                return Collections.emptyList();
        }
    }

    /**
     * Get the fighting bonus of this item against animals.
     *
     * @return fighting bonus of this item
     */
    public int fightingBonus() {
        switch (this) {
            case AXE:
                return 2;
            case CLUB:
                return 1;
            default:
                return 0;
        }
    }

    /**
     * Get the amount of resources this item can save in case of an animal attack or a catastrophic event.
     *
     * @return amount of resources this item protects
     */
    public int itemsSecured() {
        return this == Item.SHACK ? 5 : 0;
    }

    private Optional<Integer> diceSizeNeeded() {
        switch (this) {
            case HANG_GLIDER:
            case SAILING_RAFT:
                return Optional.of(6);
            default:
                return Optional.empty();
        }
    }

    private Optional<Integer> minimumDiceRollNeeded() {
        switch (this) {
            case HANG_GLIDER:
            case SAILING_RAFT:
                return Optional.of(4);
            default:
                return Optional.empty();
        }
    }

    /**
     * Get the category of this item.
     *
     * @return category of this item
     */
    public ItemCategory category() {
        switch (this) {
            case BALLON:
            case HANG_GLIDER:
            case SAILING_RAFT:
            case STEAMBOAT:
                return ItemCategory.ESCAPE;
            default:
                return ItemCategory.DEFAULT;
        }
    }

    /**
     * Parse a single word and return the item represented by the input.
     * This method is the inverse of {@link #toString}.
     *
     * @param input text
     * @return parsed item or null if not found
     */
    public static Item parse(String input) {
        switch (input) {
            case "axe":
                return AXE;
            case "club":
                return CLUB;
            case "shack":
                return SHACK;
            case "fireplace":
                return FIREPLACE;
            case "sailingraft":
                return SAILING_RAFT;
            case "hangglider":
                return HANG_GLIDER;
            case "steamboat":
                return STEAMBOAT;
            case "ballon":
                return BALLON;
            default:
                return null;
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case AXE:
                return "axe";
            case CLUB:
                return "club";
            case SHACK:
                return "shack";
            case FIREPLACE:
                return "fireplace";
            case SAILING_RAFT:
                return "sailingraft";
            case HANG_GLIDER:
                return "hangglider";
            case STEAMBOAT:
                return "steamboat";
            case BALLON:
                return "ballon";
            default:
                return super.toString();
        }
    }
}
