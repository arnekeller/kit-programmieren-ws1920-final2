package edu.kit.informatik.cardgame.model;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.Optional;

/**
 * Simple card stack that remembers its initial state and can be {@link #reset} on demand.
 * Always contains the same {@link Card#requiredAmount amount} of each card.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class CardStack {
    /**
     * Copy of the card stack used to initialize the stack.
     * {@link #reset Resetting} the card stack will restore this stack.
     */
    private final Deque<Card> stack;
    /**
     * Currently active card stack.
     * {@link #draw Drawing a card} will remove the first element.
     */
    private Deque<Card> activeStack;

    /**
     * Construct a new card stack with the specified card collection.
     * The first element will be {@link #draw drawn} first.
     *
     * @param cards (ordered) card collection
     * @throws LogicException if card stack has wrong distribution of cards
     * @see Card#requiredAmount
     */
    public CardStack(Collection<Card> cards) throws LogicException {
        if (!Arrays.stream(Card.values())
                .allMatch(card -> Collections.frequency(cards, card) == card.requiredAmount())) {
            throw new LogicException("invalid deck: missing or surplus cards");
        }
        this.stack = new ArrayDeque<>(cards);
        reset();
    }

    /**
     * Draw the next card.
     *
     * @return the card (empty if stack is empty)
     */
    public Optional<Card> draw() {
        return Optional.ofNullable(activeStack.pollFirst());
    }

    /**
     * Clear the currently active stack.
     */
    public void clearActiveStack() {
        activeStack.clear();
    }

    /**
     * Reset the currently active stack, restoring the original stack.
     */
    public void reset() {
        activeStack = new ArrayDeque<>(stack);
    }

    /**
     * @return whether the active card stack is empty
     */
    public boolean isEmpty() {
        return activeStack.isEmpty();
    }
}
