package edu.kit.informatik.cardgame.model;

/**
 * Thrown on semantically invalid user input, usually in the {@link CardGame game}.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class LogicException extends Exception {
    /**
     * Construct a new {@link LogicException} with the specified message.
     *
     * @param message error message
     */
    public LogicException(String message) {
        super(message);
    }
}
