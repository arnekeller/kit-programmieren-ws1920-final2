package edu.kit.informatik.cardgame.model;

/**
 * Item category.
 * {@link Item Game items} are categorized into two categories: normal items and items used to escape.
 *
 * @author Arne Keller
 * @version 1.0
 * @see Item#activate(CardGame, int, int)
 */
public enum ItemCategory {
    /**
     * Normal item, can not be used to escape.
     *
     * @see Item#AXE
     * @see Item#CLUB
     * @see Item#SHACK
     * @see Item#FIREPLACE
     */
    DEFAULT,
    /**
     * Item that can be used to escape and {@link CardGame#winGame win} the game.
     *
     * @see Item#SAILING_RAFT
     * @see Item#HANG_GLIDER
     * @see Item#STEAMBOAT
     * @see Item#BALLON
     */
    ESCAPE;
}
