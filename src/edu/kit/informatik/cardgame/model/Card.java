package edu.kit.informatik.cardgame.model;

import java.util.Optional;

/**
 * Game card. These can be used to {@link Inventory#build build items},
 * start a fight with an {@link CardCategory#ANIMAL animal} or clear some of the player's belongings.
 *
 * @see CardCategory
 * @author Arne Keller
 * @version 1.1
 */
public enum Card implements RequireDice {
    /**
     * Wood. Basic resource used to build many {@link Item items}.
     */
    WOOD,
    /**
     * Metal. Fundamental resource for advanced buildings.
     */
    METAL,
    /**
     * Plastic. Synthetic resource required to construct some items.
     */
    PLASTIC,
    /**
     * Spider. Will attack the player.
     */
    SPIDER,
    /**
     * Snake. Dangerous animal.
     */
    SNAKE,
    /**
     * Tiger. Strong animal.
     */
    TIGER,
    /**
     * Thunderstorm. Lifts up the player's items and takes them away. Also blows out the fireplace.
     */
    THUNDERSTORM;
    // note: new items *require* changes in category, parse and toString

    /**
     * Return value indicating that the player successfully fought against an animal.
     *
     * @see #activate(CardGame, int, int)
     * @see CardCategory#ANIMAL
     */
    public static final String SURVIVED = "survived";
    /**
     * Return value indicating that the player lost against an animal.
     *
     * @see #activate(CardGame, int, int)
     * @see CardCategory#ANIMAL
     */
    public static final String LOSE = "lose";

    /**
     * Activate this card. Behaviour depends on the {@link #category category} of this card:
     * <ul>
     *     <li>{@link CardCategory#RESOURCE Resource} cards are {@link CardGame#givePlayerCard added}
     *     to the player's inventory</li>
     *     <li>{@link CardCategory#ANIMAL Animal} cards {@link CardGame#waitForDiceRoll start} an encounter</li>
     *     <li>{@link CardCategory#CATASTROPHE Catastrophe} cards clear the player's resources
     *     and remove their fireplace</li>
     * </ul>
     *
     * @param game game to use card on
     */
    public void activate(CardGame game) {
        switch (this.category()) {
            case RESOURCE: // simply add resource cards to the player's inventory
                game.givePlayerCard(this);
                break;
            case ANIMAL: // animal cards trigger an encounter
                game.waitForDiceRoll(this);
                break;
            case CATASTROPHE:
                game.clearResources();
                game.deleteItem(Item.FIREPLACE);
                break;
            default: // implement behaviour of new card types here
                throw new IllegalStateException("encountered unknown card category!");
        }
    }

    @Override
    public Optional<String> activate(CardGame game, int size, int roll) {
        // throw if this card does not require a dice roll
        if (!this.diceSizeNeeded().isPresent()) {
            throw new IllegalStateException("can not process dice roll");
        } else if (this.diceSizeNeeded().get() != size || roll > size || roll < 1) {
            // unexpected dice size or impossible roll
            return Optional.empty();
        }
        // add the player's combat bonus against animals
        if (roll + game.getFightingBonus() >= this.minimumDiceRollNeeded().get()) {
            return Optional.of(SURVIVED);
        } else {
            // animal somehow managed to steal the entirety of the player's resources
            game.clearResources();
            return Optional.of(LOSE);
        }
    }

    /**
     * Get the category of this card.
     *
     * @return category of this card
     */
    public CardCategory category() {
        switch (this) {
            case WOOD:
            case METAL:
            case PLASTIC:
                return CardCategory.RESOURCE;
            case SPIDER:
            case SNAKE:
            case TIGER:
                return CardCategory.ANIMAL;
            case THUNDERSTORM:
                return CardCategory.CATASTROPHE;
            default:
                throw new IllegalArgumentException("no category defined for this card");
        }
    }

    /**
     * Dice size needed to {@link #activate(CardGame, int, int) activate} this card.
     *
     * @return expected dice size, empty if not requiring dice roll
     */
    private Optional<Integer> diceSizeNeeded() {
        switch (this) {
            case SPIDER:
                return Optional.of(4);
            case SNAKE:
                return Optional.of(6);
            case TIGER:
                return Optional.of(8);
            default:
                return Optional.empty();
        }
    }

    /**
     * Minimum dice roll needed to {@link #activate(CardGame, int, int) activate} this card.
     *
     * @return minimum dice roll for successful activation, empty if not requiring dice roll
     */
    private Optional<Integer> minimumDiceRollNeeded() {
        switch (this) {
            case SPIDER:
                return Optional.of(3);
            case SNAKE:
                return Optional.of(4);
            case TIGER:
                return Optional.of(5);
            default:
                return Optional.empty();
        }
    }

    /**
     * Get the exact number of cards of this type needed to initialize a card stack.
     *
     * @return amount of cards needed in a card deck
     * @see CardStack#CardStack
     */
    public int requiredAmount() {
        switch (this) {
            case WOOD:
            case METAL:
            case PLASTIC:
                return 16;
            case SPIDER:
            case SNAKE:
            case TIGER:
                return 5;
            case THUNDERSTORM:
                return 1;
            default:
                return 0;
        }
    }

    /**
     * Parse a single word and return the card represented by the input.
     * This method is the inverse of {@link #toString}.
     *
     * @param input card description (usually one word)
     * @return card object (null if input invalid)
     */
    public static Card parse(String input) {
        switch (input) {
            case "wood":
                return WOOD;
            case "metal":
                return METAL;
            case "plastic":
                return PLASTIC;
            case "spider":
                return SPIDER;
            case "snake":
                return SNAKE;
            case "tiger":
                return TIGER;
            case "thunderstorm":
                return THUNDERSTORM;
            default:
                return null;
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case WOOD:
                return "wood";
            case METAL:
                return "metal";
            case PLASTIC:
                return "plastic";
            case SPIDER:
                return "spider";
            case SNAKE:
                return "snake";
            case TIGER:
                return "tiger";
            case THUNDERSTORM:
                return "thunderstorm";
            default:
                throw new IllegalArgumentException("toString not defined for this card value");
        }
    }
}
