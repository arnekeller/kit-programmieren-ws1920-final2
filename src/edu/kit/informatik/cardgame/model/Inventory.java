package edu.kit.informatik.cardgame.model;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Objects;

/**
 * Player inventory.
 * Contains {@link CardCategory#RESOURCE resources} and {@link Item items}.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class Inventory {
    /**
     * Resources collected by the player.
     * Obviously only contains {@link CardCategory#RESOURCE resource} cards.
     */
    private final Deque<Card> resources = new ArrayDeque<>();
    /**
     * Items built by the player in chronological order of creation.
     */
    private final List<Item> items = new ArrayList<>();

    /**
     * Add a resource card to this inventory.
     *
     * @param resourceCard resource card
     * @throws IllegalArgumentException if the provided card is not a resource card
     */
    public void addResource(Card resourceCard) throws IllegalArgumentException {
        if (resourceCard.category() != CardCategory.RESOURCE) {
            throw new IllegalArgumentException("card is not a resource");
        }
        resources.add(resourceCard);
    }

    /**
     * Get the resources stored in this inventory in chronological order of acquisition.
     *
     * @return resources owned by the player
     */
    public Collection<Card> getResources() {
        // have to copy here: caller does not expect an auto-updating collection
        return new ArrayDeque<>(resources);
    }

    /**
     * Clear player resources, {@link Item#itemsSecured leaving} a few items
     * in the {@link Item#SHACK shack} if one is built.
     */
    public void clearResources() {
        // calculate the resources saved by player items
        final int keepLastResources = items.stream().mapToInt(Item::itemsSecured).sum();
        while (resources.size() > keepLastResources) {
            // remove resources that were picked up earlier first
            resources.removeFirst();
        }
    }

    /**
     * Attempt to build the specified item.
     *
     * @param item item to build
     * @return whether the item could be built (false if missing resources or items)
     */
    public boolean build(Item item) {
        if (!canBuild(item)) {
            return false;
        }
        // remove used resources
        for (final Card resource : item.resourcesNeeded()) {
            // resources picked up last get used up first
            resources.removeLastOccurrence(resource);
        }
        items.add(item);
        return true;
    }

    /**
     * Get the items stored in this inventory in chronological order of creation.
     *
     * @return items owned by the player
     */
    public List<Item> getItems() {
        // have to copy here: caller does not expect an auto-updating list
        return new ArrayList<>(items);
    }

    /**
     * Check whether this inventory contains a specific item.
     *
     * @param item single item
     * @return whether this inventory contains that item
     */
    public boolean contains(Item item) {
        return items.contains(item);
    }

    /**
     * Remove an item, if it exists.
     *
     * @param item item to remove
     */
    public void removeItem(Item item) {
        items.remove(item);
    }

    /**
     * Check whether the player can build the specified item with the currently available resources and items.
     *
     * @param item item to build
     * @return whether that item can be built
     */
    public boolean canBuild(Item item) {
        // check whether item is already built
        if (items.contains(item)) {
            return false;
        }
        // make sure all of the required items are already built
        if (!items.containsAll(item.itemsNeededToBuild())) {
            return false;
        }
        // get the needed resources and check whether this inventory contains all of them
        final Card[] resourcesNeeded = item.resourcesNeeded();
        for (final Card resource : resources) {
            for (int j = 0; j < resourcesNeeded.length; j++) {
                if (resourcesNeeded[j] != null && resourcesNeeded[j] == resource) {
                    // overwrite entries with null if available in inventory
                    resourcesNeeded[j] = null;
                    break; // process next available resource
                }
            }
        }
        // now just check whether all of the entries are null
        return Arrays.stream(resourcesNeeded).allMatch(Objects::isNull);
    }

    /**
     * Clear the inventory, deleting all resources and items.
     */
    public void clear() {
        resources.clear();
        items.clear();
    }
}
