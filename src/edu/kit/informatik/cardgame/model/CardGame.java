package edu.kit.informatik.cardgame.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Simple card game. Features:
 * <ul>
 *     <li>100% deterministic (user tells game results of dice rolls)</li>
 *     <li>64! different card stacks (including functionally equivalent stacks)</li>
 *     <li>8 buildable {@link Item item} items</li>
 *     <li>7 exciting {@link Card card} types</li>
 *     <li>4 challenging game phases: scavenge (draw and build), encounter (fight), endeavor (escape) and end</li>
 *     <li>3 dice needed (size 4, 6 and 8)</li>
 *     <li>2 common game endings ({@link Phase#LOST lost} and {@link Phase#WON win})</li>
 *     <li>1 player only</li>
 *     <li>0 skill required</li>
 * </ul>
 *
 * @author Arne Keller
 * @version 1.2
 */
public class CardGame {
    /**
     * Card stack used, null if game not yet started.
     */
    private CardStack cardStack;
    /**
     * Inventory of the player, containing {@link Card resources} and {@link Item items}, null if game not yet started.
     */
    private Inventory inventory;
    /**
     * Current game phase, null if game not yet started.
     */
    private Phase phase;
    /**
     * Object currently requiring dice roll, null if not awaiting dice roll.
     *
     * @see Card
     * @see Item
     */
    private RequireDice toActivateOnDiceRoll;

    /**
     * Start a new game with the specified stack of cards. First checks whether the specified stack is correctly
     * composed: every card should exist exactly as often as {@link Card#requiredAmount required}.
     *
     * @param cardStack stack of cards to use, where the first element is the first to take
     * @throws LogicException if card stack has wrong distribution of cards
     * @return whether a new game was started
     */
    public boolean start(Collection<Card> cardStack) throws LogicException {
        if (gameActive()) {
            // do not start a new game if one is active already
            return false;
        }
        this.cardStack = new CardStack(cardStack);
        this.inventory = new Inventory();
        this.phase = Phase.SCAVENGE;
        // clear player inventory (relevant when restarting game)
        reset();
        return true;
    }

    /**
     * Draw a new card. Will automatically process the card action ({@link Card#activate}).
     *
     * @return the card
     * @throws LogicException if no card to draw exists or phase is not scavenge
     */
    public Card draw() throws LogicException {
        if (cardStack == null) {
            throw new LogicException("no card stack to draw from exists");
        } else if (phase != Phase.SCAVENGE) {
            throw new LogicException("can only draw cards in scavenge phase");
        }
        final Card card = cardStack.draw().orElseThrow(() -> new LogicException("no card to draw exists"));
        card.activate(this);
        return card;
    }

    /**
     * Give the player a card.
     *
     * @param card (resource) card
     * @see Inventory#addResource
     */
    public void givePlayerCard(Card card) {
        this.inventory.addResource(card);
    }

    /**
     * Wait for a dice roll to activate the specified object. {@link #rollDice} has to be used next.
     *
     * @param requireDice something that requires a dice roll
     */
    public void waitForDiceRoll(RequireDice requireDice) {
        this.toActivateOnDiceRoll = requireDice;
        phase = Phase.AWAITING_DICE_ROLL;
    }

    /**
     * Delete the player's resources.
     *
     * @see Inventory#clearResources
     */
    public void clearResources() {
        this.inventory.clearResources();
    }

    /**
     * Delete one item the player owns.
     *
     * @param item item to remove
     * @see Inventory#removeItem
     */
    public void deleteItem(Item item) {
        this.inventory.removeItem(item);
    }

    /**
     * Roll a dice with the specified size and result.
     *
     * @param size dice size
     * @param roll dice result
     * @return result of the dice roll (see {@link RequireDice#activate})
     * @throws LogicException if not expecting dice roll or dice roll is invalid
     */
    public String rollDice(int size, int roll) throws LogicException {
        if (phase != Phase.AWAITING_DICE_ROLL) {
            throw new LogicException("not expecting dice roll");
        }
        // compute the result of the dice roll
        final Optional<String> result = toActivateOnDiceRoll.activate(this, size, roll);
        if (result.isPresent()) { // valid dice roll
            // leave encounter/endeavor phase (only if player didn't win)
            if (phase == Phase.AWAITING_DICE_ROLL) {
                phase = Phase.SCAVENGE;
            }
            // no longer waiting
            toActivateOnDiceRoll = null;
            return result.get();
        } else {
            // invalid user input
            throw new LogicException("invalid roll dice input (wrong size or impossible roll)");
        }
    }

    /**
     * @return the player's fighting bonus against {@link CardCategory#ANIMAL animals}
     */
    public int getFightingBonus() {
        return inventory.getItems().stream().mapToInt(Item::fightingBonus).max().orElse(0);
    }

    /**
     * Attempt to build the specified item. Will automatically {@link Item#activate(CardGame) activate} the item.
     *
     * @param item item to build
     * @return build result
     * @throws LogicException if in wrong phase, item already built or missing resources/items
     */
    public String build(Item item) throws LogicException {
        if (item == null) {
            throw new IllegalArgumentException("can not build null item");
        } else if (phase != Phase.SCAVENGE) {
            throw new LogicException("can only build in scavenge phase");
        } else if (inventory.contains(item)) {
            throw new LogicException("already built");
        } else {
            if (!inventory.build(item)) {
                throw new LogicException("can not build item: missing resources/items");
            } // else: successfully built and added to the inventory
            // make sure to activate the item
            return item.activate(this);
        }
    }

    /**
     * Check whether this game ever received a {@link #start start} command.
     *
     * @return whether the game was ever started
     */
    public boolean gameStarted() {
        return phase != null;
    }

    /**
     * Check whether the player can do any of these actions:
     * <ul>
     *     <li>{@link #draw draw} a card</li>
     *     <li>{@link #build build} any item</li>
     *     <li>{@link #rollDice roll} a dice</li>
     * </ul>
     *
     * @return whether the game is currently active (player can still do an action)
     */
    public boolean gameActive() {
        return gameStarted() && phase != Phase.WON && phase != Phase.LOST;
    }

    /**
     * Check whether the player lost, updating the game state accordingly.
     *
     * @see #gameLost
     */
    private void checkLost() {
        // obviously, player can only lose after starting the game
        if (gameStarted()
                // can not draw new cards
                && (cardStack == null || cardStack.isEmpty())
                // can not roll dice
                && phase != Phase.AWAITING_DICE_ROLL
                // can not build item
                && Arrays.stream(Item.values()).noneMatch(inventory::canBuild)
                // can not lose after winning..
                && phase != Phase.WON) {
            endGame();
            phase = Phase.LOST;
        }
    }

    /**
     * End the game by forcing the player to win.
     */
    public void winGame() {
        endGame();
        phase = Phase.WON;
    }

    /**
     * End the current game, {@link CardStack#clearActiveStack deleting} the active card stack.
     * Player resources and items are not deleted.
     */
    private void endGame() {
        cardStack.clearActiveStack();
    }

    /**
     * Check whether the active game is lost.
     * A game is lost if no more player actions (see {@link #gameActive}) are possible and
     * the player did not {@link Phase#WON win}.
     *
     * @return whether the active game is lost
     */
    public boolean gameLost() {
        checkLost();
        return phase == Phase.LOST;
    }

    /**
     * Get the resources available for {@link #build building} in chronological order of acquisition.
     *
     * @return usable resources
     * @throws LogicException if game is not {@link #gameStarted started}
     * @see Inventory#getResources
     */
    public Collection<Card> getResources() throws LogicException {
        if (!gameStarted()) {
            throw new LogicException("can not get resources: game not started");
        }
        return inventory.getResources();
    }

    /**
     * Get the items already {@link #build built} in chronological order of construction.
     *
     * @return items owned by the player
     * @throws LogicException if game is not {@link #gameStarted started}
     * @see Inventory#getItems
     */
    public List<Item> getItems() throws LogicException {
        if (!gameStarted()) {
            throw new LogicException("can not get buildings: game not started");
        }
        return inventory.getItems();
    }

    /**
     * Get the currently {@link #build buildable} items.
     *
     * @return items currently buildable
     * @throws LogicException if items can not be built in current game phase
     */
    public Set<Item> getBuildableItems() throws LogicException {
        if (!gameStarted()) {
            throw new LogicException("can not get buildable items: game not started");
        } else if (phase != Phase.SCAVENGE) {
            throw new LogicException("can not get buildable items: awaiting dice roll");
        }
        return Arrays.stream(Item.values()).filter(inventory::canBuild).collect(Collectors.toSet());
    }

    /**
     * Reset the game, restoring the {@link #cardStack original card stack} and clearing the player's inventory.
     *
     * @throws LogicException if game was never started
     */
    public void reset() throws LogicException {
        if (!gameStarted()) {
            throw new LogicException("can not reset a game that is not started!");
        }
        this.cardStack.reset();
        this.inventory.clear();
        this.toActivateOnDiceRoll = null;
        this.phase = Phase.SCAVENGE;
    }

    /**
     * Game phase. A game starts in the {@link Phase#SCAVENGE scavenge} phase and ends with the player
     * {@link Phase#WON winning} or {@link Phase#LOST losing}.
     *
     * @author Arne Keller
     * @version 1.1
     */
    private enum Phase {
        /**
         * Player can draw cards and build items.
         *
         * @see #draw
         * @see #build
         * @see #getBuildableItems
         */
        SCAVENGE,
        /**
         * Player has to fight an animal (encounter) or attempt to escape (endeavor) using {@link #rollDice(int, int)}.
         *
         * @see CardCategory#ANIMAL
         * @see ItemCategory#ESCAPE
         */
        AWAITING_DICE_ROLL,
        /**
         * Player won.
         */
        WON,
        /**
         * Player lost.
         *
         * @see #gameLost
         */
        LOST
    }
}
