package edu.kit.informatik.cardgame.model;

import java.util.Optional;

/**
 * Objects of classes implementing this interface optionally require a dice to be rolled.
 *
 * @author Arne Keller
 * @version 2.0
 */
public interface RequireDice {
    /**
     * Activate this object in a game if the dice roll is lucky enough.
     *
     * @param game card game to use
     * @param size size of the dice rolled
     * @param roll result of the dice roll
     * @return activation result (empty if dice roll is incorrect)
     */
    Optional<String> activate(CardGame game, int size, int roll);
}
