package edu.kit.informatik.cardgame.model;

/**
 * Card category. {@link Card Game cards} are categorized into three categories depending on their properties.
 *
 * @author Arne Keller
 * @version 1.0
 * @see Card#activate(CardGame)
 */
public enum CardCategory {
    /**
     * Basic resource card, harmless.
     *
     * @see Card#WOOD
     * @see Card#METAL
     * @see Card#PLASTIC
     * @see Item#resourcesNeeded
     */
    RESOURCE,
    /**
     * Animal card, can attack player.
     *
     * @see Card#SNAKE
     * @see Card#SPIDER
     * @see Card#TIGER
     * @see Card#activate(CardGame, int, int)
     */
    ANIMAL,
    /**
     * Catastrophic card causing clearance of collected cards and fireplace.
     *
     * @see Card#THUNDERSTORM
     * @see CardGame#clearResources
     */
    CATASTROPHE
}
